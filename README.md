# errbot-tracim-backend

![pipeline_build_status](https://framagit.org/inkhey/trasc/badges/master/pipeline.svg)
![PyPI - License](https://img.shields.io/pypi/l/errbot-tracim-backend)
![PyPI - Status](https://img.shields.io/pypi/status/errbot-tracim-backend)
[![PyPI](https://img.shields.io/pypi/v/errbot-tracim-backend) ![PyPI - Python Version](https://img.shields.io/pypi/pyversions/errbot-tracim-backend) ![PyPI](https://img.shields.io/pypi/dm/errbot-tracim-backend)](https://pypi.org/project/errbot-tracim-backend/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
![mypy](https://img.shields.io/badge/mypy-checked-blueviolet)

Quick and Dirty Errbot Backend for tracim.

## Install

### From pypi

```sh
$ pip install errbot-tracim-backend
```

### From source

- clone this repository
- `pip install -e "."`

## Usage
