# add black for python 3.6+
import os
import sys

from setuptools import find_packages
from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))
try:
    documentation = open(os.path.join(here, "README.md")).read()
except IOError:
    documentation = ""
except UnicodeDecodeError:
    documentation = ""

install_requires = [
    "aiohttp",
    "html2text",
    "trasync"
]
tests_require = ["pytest", "aioresponses"]
devtools_require = ["flake8", "isort", "mypy", "pre-commit", "commitizen"]
if sys.version_info.major == 3 and sys.version_info.minor >= 6:
    devtools_require.append("black")
    install_requires.append("dataclasses")
if sys.version_info.major == 3 and sys.version_info.minor <= 7:
    install_requires.append("typing_extensions")

setup(
    name="errbot-tracim-backend",
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    description="Asynchronous Tracim API wrapper",
    url="https://framagit.org/inkhey/errbot-tracim-backend",
    long_description=documentation,
    long_description_content_type='text/markdown',
    author="Guénaël Muller",
    author_email="inkey@inkey-art.net",
    keywords=["tracim"],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        'License :: OSI Approved :: MIT License',
    ],
    packages=find_packages(include=["errbot-tracim-backend", "errbot-tracim-backend.*"]),
    install_requires=install_requires,
    python_requires=">= 3.5",
    include_package_data=True,
    extras_require={"testing": tests_require, "dev": tests_require + devtools_require},
)
