import asyncio
import json
import queue
from typing import Optional, List

import aiohttp
from errbot.backends.base import Person, Room, RoomOccupant, Message, ONLINE
from errbot.core import ErrBot
from html2text import html2text

import trasync
from trasync import Trasync



class TracimPerson(Person):
    def __init__(self, public_name: str, username: Optional[str], user_id: int, email:str = None):
        super().__init__()
        self.public_name = public_name
        self.username = username
        self.user_id = user_id
        self._email = email

    @property
    def person(self):
        """Tracim user identifier"""
        return "<user:{}>".format(str(self.user_id))

    @property
    def client(self):
        """Client specific information, not useful here"""
        return 'Tracim'

    @property
    def nick(self):

        return self.username or self.public_name

    @property
    def aclattr(self) -> str:
        return self.person

    @property
    def fullname(self):
        return self.public_name

    @property
    def email(self) -> str:
        return self._email or ""


class TracimRoom(Room):
    """
    In tracim, each content is a chatroom.
    """
    def __init__(self, workspace_id: int, content_id: int, description: str, name: str):
        super().__init__()
        self.workspace_id = workspace_id
        self.content_id = content_id
        self.description = description
        self.name = name

    @property
    def exists(self) -> bool:
        raise NotImplementedError("TODO")

    @property
    def joined(self) -> bool:
        raise NotImplementedError("TODO")

    @property
    def create(self) -> None:
        raise NotImplementedError("TODO")

    @property
    def topic(self) -> str:
        return self.description

    @property
    def occupants(self) -> List[RoomOccupant]:
        raise NotImplementedError("TODO")

    @property
    def invite(self):
        raise NotImplementedError("TODO")


class TracimRoomOccupant(TracimPerson, RoomOccupant):
    def __init__(
            self,
            public_name: str,
            username: Optional[str],
            user_id: int,
            room: TracimRoom,
            email: str = None,
    ):
        TracimPerson.__init__(
            self,
            public_name=public_name,
            username=username,
            user_id=user_id,
            email=email
        )
        RoomOccupant.__init__(self)
        self._room = room

    @property
    def room(self):
        return self._room

class TracimBackend(ErrBot):
    def __init__(self, config):
        super().__init__(config)
        identity = config.BOT_IDENTITY
        self.login = identity['login']
        self.password = identity['password']
        self.base_url = identity['base_url']
        self.connected = False
        self.conn = self.create_connection()

    def create_connection(self):
        auth = aiohttp.BasicAuth(
            login=self.login,
            password=self.password
        )
        client_session = aiohttp.ClientSession(auth=auth)
        return Trasync(session=client_session, base_url=self.base_url)

    def send_message(self, msg: Message):
        super().send_message(msg)
        if not isinstance(msg.to, TracimRoom):
            raise NotImplementedError("TODO")
        dest = msg.to  # type: TracimRoom
        asyncio.run_coroutine_threadsafe(
            self.conn.contents.comments.create(
                workspace_id=dest.workspace_id,
                content_id=dest.content_id,
                raw_content=msg.body
            ), self.loop
        )

    def change_presence(self, status: str = ONLINE, message: str = "") -> None:
        raise NotImplementedError("TODO")

    def build_reply(
        self,
        msg: Message,
        text: str = None,
        private: bool = False,
        threaded: bool = False,
    ):
        response = self.build_message(text)
        if not text:
            raise NotImplementedError("TODO")
        if private or threaded:
            raise NotImplementedError("TODO")
        if isinstance(msg.frm, TracimRoomOccupant):
            origin_room = msg.frm.room # type: TracimRoom
            response.to = origin_room
            return response
        else:
            raise NotImplementedError("TODO")

    def query_room(self, room: str) -> Room:
        raise NotImplementedError("TODO")

    def mode(self) -> str:
        return "tracim"

    def disconnect(self):
        self.connected = False

    def serve_forever(self) -> None:
        async def main():
            async with self.conn.users.get_live_messages(user_id=1) as event_source:
                try:
                    self.connected = True
                    self.connect_callback()
                    async for event in event_source:
                        if self.connected is False:
                            break
                        if event.type == 'message':
                            self._message_event_handler(event.data)
                except ConnectionError:
                    pass
            await self.conn.session.close()
        self.loop = asyncio.get_event_loop()
        self.loop.run_until_complete(main())

    def _message_event_handler(self, message_event):
        data = json.loads(message_event)
        if data['event_type'] == 'content.created.comment':
            fields = data['fields']
            content = fields['content']
            workspace = fields['workspace']
            author = content['author']
            to = TracimRoom(
                content_id=content['parent_id'],
                workspace_id=workspace['workspace_id'],
                description='',
                name=content['parent_label'],
            )
            frm = TracimRoomOccupant(
                public_name=author['public_name'],
                user_id=author['user_id'],
                username=author['username'],
                room = to
            )
            body = html2text(content.get('raw_content')).strip()

            msg = Message(
                frm=frm,
                to=to,
                body=body,
            )
            self.callback_message(
                msg
            )

    def build_identifier(self, text_representation: str):
        raise NotImplementedError("TODO")

    def rooms(self):
        raise NotImplementedError("TODO")
